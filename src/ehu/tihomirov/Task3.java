package ehu.tihomirov;

public class Task3 {

    public int greatestCommonDivisor(int first, int second) {
        while (first != 0 && second != 0){
            if (first > second){
                first = first % second;
            } else {
                second = second % first;
            }
        }
        return first + second;
    }

    public int greatestCommonDivisor(int first, int second, int third) {
        return this.greatestCommonDivisor(third, this.greatestCommonDivisor(first, second));
    }
}
