package ehu.tihomirov;

public class Task2 {

    public int greatestCommonDivisor(int first, int second) {
        while (first != 0 && second != 0){
            if (first > second){
                first = first % second;
            } else {
                second = second % first;
            }
        }
        return first + second;
    }

    public int greatestCommonDivisor(int first, int second, int third, int fourth) {
        return this.greatestCommonDivisor(
                this.greatestCommonDivisor(first, second),
                this.greatestCommonDivisor(third, fourth)
        );
    }
}
