package ehu.tihomirov;

public class Task4 {
    public int factorial(int number) {
        int result = 1;
        for (int i = 2; i <= number; i++){
            result *= i;
        }
        return result;
    }

    public int sumOfFactorials(int first, int second) {
        return factorial(first) + factorial(second);
    }
}
