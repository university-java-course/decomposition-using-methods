package ehu.tihomirov;

public class Task1 {

    public int greatestCommonDivisor(int first, int second) {
        while (first != 0 && second != 0){
            if (first > second){
                first = first % second;
            } else {
                second = second % first;
            }
        }
        return first + second;
    }

    public int smallestCommonMultiple(int first, int second) {
        return first * second / this.greatestCommonDivisor(first, second);
    }
}
