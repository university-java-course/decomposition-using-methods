package ehu.tihomirov;

public class Main {
    public static void main(String[] args) {
        Task1 task1 = new Task1();

        System.out.println(task1.greatestCommonDivisor(15, 5));
        System.out.println(task1.smallestCommonMultiple(4, 3));

        Task2 task2 = new Task2();
        System.out.println(task2.greatestCommonDivisor(5, 15, 30, 20));

        Task3 task3 = new Task3();
        System.out.println(task3.greatestCommonDivisor(10, 10, 100));

        Task4 task4 = new Task4();
        System.out.println(task4.sumOfFactorials(4, 5));

        Task5 task5 = new Task5();
        System.out.println(task5.getSecondGreatestNumber(new int[]{0, 3, 4, 6, 3, 1, 7, 2, 57, 45, 57,74,74, 3}));
    }
}
