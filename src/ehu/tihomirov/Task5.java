package ehu.tihomirov;

public class Task5 {

    private void _swap(int[] array, int i, int j){
        array[i] = array[i] ^ array[j];
        array[j] = array[i] ^ array[j];
        array[i] = array[i] ^ array[j];
    }
    private void _sort(int[] array){
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if(array[i] > array[j]){
                    this._swap(array, i, j);
                }
            }
        }

    }

    private boolean _inArray(int value, int[] array){
        for (int number : array) {
            if (number == value) {
                return true;
            }
        }
        return false;
    }

    private int[] _removeDuplicates(int[] array){
        int[] copyArray = array.clone();
        int[] set = new int[copyArray.length];
        int size = 0;
        for (int number : copyArray) {
            if (!_inArray(number, set)) {
                set[size] = number;
                size++;
            }
        }
        return set;
    }

    public int getSecondGreatestNumber(int[] array){
        int[] copyArray = this._removeDuplicates(array);
        this._sort(copyArray);

        return copyArray[1];
    }
}
